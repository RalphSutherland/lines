#include "measure.h"
#include "zls_io.h"


#ifdef PTHREADS

#include  <pthread.h>

static int gSweepLoadLock = 0;
static int gLoadProcessIDs[NTHREADS];

pthread_mutex_t gSweepLoadMutex    = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  gSweepLoadCond     = PTHREAD_COND_INITIALIZER;
pthread_cond_t  gNextSweepLoadCond = PTHREAD_COND_INITIALIZER;

#endif
static Integer fileLoadIndex;

static char * inputdata = NULL;


#define SAMPLE1
    //#define DOWNSAMPLE2

int CopyZoneIntoData( Q3DArr d, UInt32 nx, UInt32 ny, UInt32 nz,
                     void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ,
                     UInt32 zoneNX0,  UInt32 zoneNY0,  UInt32 zoneNZ0,  UInt32 zoneSample);

int CopyZoneIntoData( Q3DArr d, UInt32 nx, UInt32 ny, UInt32 nz,
                     void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ,
                     UInt32 zoneNX0,  UInt32 zoneNY0,  UInt32 zoneNZ0,  UInt32 zoneSample){
    int err = noErr;

    UInt32  i,  j,  k; // coords in d
    UInt32 ix, jy, kz; // coords in zdata

    Real32 min = 1e38;
    Real32 max = -1e38;

    if (( d != NULL) && (zdata != NULL)) {
#ifdef DEBUG
        zlsio_Printf(  " Copying into d\n");
#endif // DEBUG

        if ( zoneSample == 4){

            float * srcd = ( float * )zdata;

#ifdef DOWNSAMPLE2
            UInt32 id, jd, kd;
#endif // DOWNSAMPLE2

            for ( kz = 0; kz < zoneNZ; kz++) {
                k = kz + zoneNZ0;
#ifdef DOWNSAMPLE2
                kd = k >> 1;
#endif // DOWNSAMPLE2
                for ( jy = 0; jy < zoneNY; jy++) {
                    j = jy + zoneNY0;
#ifdef DOWNSAMPLE2
                    jd = j >> 1;
#endif // DOWNSAMPLE2
                    for ( ix = 0; ix < zoneNX; ix++) {
                        i = ix + zoneNX0;
#ifdef DOWNSAMPLE2
                        id = i >> 1;
                        if ( ((ix%2)==0) && ((jy%2)==0) && ((kz%2)==0) ) {
                            d[kd][jd][id]  = 0.125*srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        } else {
                            d[kd][jd][id] += 0.125*srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        }
#endif // DOWNSAMPLE2

#ifdef SAMPLE1
                        Real32 val =  srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        max = (max < val)? val: max;
                        min = (min > val)? val: min;
                        d[k][j][i] = val;
#endif //SAMPLE1


                    } // i
                } // j
            } // k

        }  // zoneSample == 4

            //        printf(" max min % g %g \n", max, min);

        if ( zoneSample == 8){

            double * srcd = ( double * )zdata;

            for ( kz = 0; kz < zoneNZ; kz++) {
                k  = kz + zoneNZ0;
                for ( jy = 0; jy < zoneNY; jy++) {
                    j = jy + zoneNY0;
                    for ( ix = 0; ix < zoneNX; ix++) {
                        i = ix + zoneNX0;

#ifdef SAMPLE1
                        d[k][j][i] = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif //SAMPLE1

                            // just a straight copy for now

                    } // i
                } // j
            } // k

        }  // zoneSample == 8

    } else {
        err = memErr;
    }

    return (err);

}

int AddZoneIntoData( Q3DArr d, UInt32 nx, UInt32 ny, UInt32 nz,
                    void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ,
                    UInt32 zoneNX0,  UInt32 zoneNY0,  UInt32 zoneNZ0,  UInt32 zoneSample);

int AddZoneIntoData( Q3DArr d, UInt32 nx, UInt32 ny, UInt32 nz,
                    void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ,
                    UInt32 zoneNX0,  UInt32 zoneNY0,  UInt32 zoneNZ0,  UInt32 zoneSample){
    int err = noErr;

    UInt32  i,  j,  k; // coords in d
    UInt32 ix, jy, kz; // coords in zdata

    if (( d != NULL) && (zdata != NULL)) {
#ifdef DEBUG
        zlsio_Printf(  " Copying into d\n");
#endif // DEBUG

        if ( zoneSample == 4){

            float * srcd = ( float * )zdata;

#ifdef DOWNSAMPLE2
            UInt32 id, jd, kd;
#endif // DOWNSAMPLE2

            for ( kz = 0; kz < zoneNZ; kz++) {
                k = kz + zoneNZ0;
#ifdef DOWNSAMPLE2
                kd = k >> 1;
#endif // DOWNSAMPLE2
                for ( jy = 0; jy < zoneNY; jy++) {
                    j = jy + zoneNY0;
#ifdef DOWNSAMPLE2
                    jd = j >> 1;
#endif // DOWNSAMPLE2
                    for ( ix = 0; ix < zoneNX; ix++) {
                        i = ix + zoneNX0;
#ifdef DOWNSAMPLE2
                        id = i >> 1;
                        d[kd][jd][id] += 0.125*srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif // DOWNSAMPLE2

#ifdef SAMPLE1
                        d[k][j][i] += srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif //SAMPLE1


                    } // i
                } // j
            } // k

        }  // zoneSample == 4

        if ( zoneSample == 8){

            double * srcd = ( double * )zdata;

            for ( kz = 0; kz < zoneNZ; kz++) {
                k  = kz + zoneNZ0;
                for ( jy = 0; jy < zoneNY; jy++) {
                    j = jy + zoneNY0;
                    for ( ix = 0; ix < zoneNX; ix++) {
                        i = ix + zoneNX0;

#ifdef SAMPLE1
                        d[k][j][i] += srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif //SAMPLE1


                    } // i
                } // j
            } // k

        }  // zoneSample == 8

    } else {
        err = memErr;
    }

    return (err);

}

int MultiplyZoneIntoData( Q3DArr d, UInt32 nx, UInt32 ny, UInt32 nz,
                         void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ,
                         UInt32 zoneNX0,  UInt32 zoneNY0,  UInt32 zoneNZ0,  UInt32 zoneSample);

int MultiplyZoneIntoData( Q3DArr d, UInt32 nx, UInt32 ny, UInt32 nz,
                         void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ,
                         UInt32 zoneNX0,  UInt32 zoneNY0,  UInt32 zoneNZ0,  UInt32 zoneSample){
    int err = noErr;

    UInt32  i,  j,  k; // coords in d
    UInt32 ix, jy, kz; // coords in zdata

    if (( d != NULL) && (zdata != NULL)) {
#ifdef DEBUG
        zlsio_Printf(  " Copying into d\n");
#endif // DEBUG

        if ( zoneSample == 4){

            float * srcd = ( float * )zdata;

#ifdef DOWNSAMPLE2
            UInt32 id, jd, kd;
#endif // DOWNSAMPLE2

            for ( kz = 0; kz < zoneNZ; kz++) {
                k = kz + zoneNZ0;
#ifdef DOWNSAMPLE2
                kd = k >> 1;
#endif // DOWNSAMPLE2
                for ( jy = 0; jy < zoneNY; jy++) {
                    j = jy + zoneNY0;
#ifdef DOWNSAMPLE2
                    jd = j >> 1;
#endif // DOWNSAMPLE2
                    for ( ix = 0; ix < zoneNX; ix++) {
                        i = ix + zoneNX0;
#ifdef DOWNSAMPLE2
                        id = i >> 1;
                        d[kd][jd][id] *= 0.125*srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif // DOWNSAMPLE2

#ifdef SAMPLE1
                        d[k][j][i] *= 0.5*srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif //SAMPLE1


                    } // i
                } // j
            } // k

        }  // zoneSample == 4

        if ( zoneSample == 8){

            double * srcd = ( double * )zdata;

            for ( kz = 0; kz < zoneNZ; kz++) {
                k  = kz + zoneNZ0;
                for ( jy = 0; jy < zoneNY; jy++) {
                    j = jy + zoneNY0;
                    for ( ix = 0; ix < zoneNX; ix++) {
                        i = ix + zoneNX0;

#ifdef SAMPLE1
                        d[k][j][i] *= 0.5*srcd[ ix + zoneNX*( jy + zoneNY*kz)];
#endif //SAMPLE1

                            // just a straight copy for now

                    } // i
                } // j
            } // k

        }  // zoneSample == 8

    } else {
        err = memErr;
    }

    return (err);

}



int expZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample);
int expZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample)
{
    int err = noErr;
    SInt32 ix, jy, kz; // coords in zdata
    if (zdata != NULL) {

        if ( zoneSample == 4){
            Real32 * srcd = ( Real32 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real32 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = powf(10.0f, x);
                    } // i
                } // j
            } // k
        }  // zoneSample == 4

        if ( zoneSample == 8){
            Real64 * srcd = ( Real64 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real64 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = pow(10.0, x);
                    } // i
                } // j
            } // k
        }  // zoneSample == 8

    } else {
        err = memErr;
    }
    return (err);
}

int logZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample);
int logZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample)
{
    int err = noErr;
    SInt32 ix, jy, kz; // coords in zdata
    if (zdata != NULL) {

        if ( zoneSample == 4){
            Real32 * srcd = ( Real32 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real32 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = log10f(x);
                    } // i
                } // j
            } // k
        }  // zoneSample == 4

        if ( zoneSample == 8){
            Real64 * srcd = ( Real64 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real64 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = log10(x);
                    } // i
                } // j
            } // k
        }  // zoneSample == 8

    } else {
        err = memErr;
    }
    return (err);
}

int sqrZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample);
int sqrZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample)
{
    int err = noErr;
    SInt32 ix, jy, kz; // coords in zdata
    if (zdata != NULL) {

        if ( zoneSample == 4){
            Real32 * srcd = ( Real32 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real32 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = x*x;
                    } // i
                } // j
            } // k
        }  // zoneSample == 4

        if ( zoneSample == 8){
            Real64 * srcd = ( Real64 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real64 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = x*x;
                    } // i
                } // j
            } // k
        }  // zoneSample == 8

    } else {
        err = memErr;
    }
    return (err);
}

int sqrtZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample);
int sqrtZone(void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample)
{
    int err = noErr;
    SInt32 ix, jy, kz; // coords in zdata
    if (zdata != NULL) {
        if ( zoneSample == 4){
            Real32 * srcd = ( Real32 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real32 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = sqrtf(x);
                    } // i
                } // j
            } // k

        }  // zoneSample == 4

        if ( zoneSample == 8){
            Real64 * srcd = ( Real64 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real64 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = sqrt(x);
                    } // i
                } // j
            } // k
        }  // zoneSample == 8
    } else {
        err = memErr;
    }
    return (err);
}

int scaleZone(Real s, void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample);
int scaleZone(Real s, void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample)
{
    int err = noErr;
    SInt32 ix, jy, kz; // coords in zdata
    if (zdata != NULL) {
        if ( zoneSample == 4){
            Real32 * srcd = ( Real32 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real32 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = s*x;
                    } // i
                } // j
            } // k

        }  // zoneSample == 4

        if ( zoneSample == 8){
            Real64 * srcd = ( Real64 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real64 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] =  s*x;
                    } // i
                } // j
            } // k
        }  // zoneSample == 8
    } else {
        err = memErr;
    }
    return (err);
}


int powZone(Real a, void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample);
int powZone(Real a, void * zdata,   UInt32 zoneNX, UInt32  zoneNY,  UInt32 zoneNZ, UInt32 zoneSample)
{
    int err = noErr;
    SInt32 ix, jy, kz; // coords in zdata
    if (zdata != NULL) {
        if ( zoneSample == 4){
            Real32 * srcd = ( Real32 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real32 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = powf(x, a);
                    } // i
                } // j
            } // k

        }  // zoneSample == 4

        if ( zoneSample == 8){
            Real64 * srcd = ( Real64 * )zdata;
            for ( kz = 0; kz < zoneNZ; kz++) {
                for ( jy = 0; jy < zoneNY; jy++) {
                    for ( ix = 0; ix < zoneNX; ix++) {
                        Real64 x = srcd[ ix + zoneNX*( jy + zoneNY*kz)];
                        srcd[ ix + zoneNX*( jy + zoneNY*kz)] = pow(x, a);
                    } // i
                } // j
            } // k
        }  // zoneSample == 8
    } else {
        err = memErr;
    }
    return (err);
}


int ScaleData( Q3DArr df, void * d, size_t nx, size_t ny, size_t nz)
{

    int i, j, k;
    unsigned long nk, njk;

	unsigned char * U8data = NULL;
	unsigned short * U16data = NULL;
	short * S16data = NULL;
	int   * S32data = NULL;
	float * F32data = NULL;
	double * F64data = NULL;

	if ( inBitsPerPix == k8bitUIntInput ){
		U8data = (unsigned char *) d;

		for(k = 0;k<nz;k++){
			nk = ny*k;
			for(j = 0;j<ny;j++){
				njk = nx*(j+nk);
				for(i = 0;i<nx;i++){
					df[k][j][i] = (inScale*U8data[ i+njk])+inOffset;
				}
			}
		}
	}

	if ( inBitsPerPix == k16bitSIntInput ){
		S16data = (short *) d;

		for(k = 0;k<nz;k++){
			nk = ny*k;
			for(j = 0;j<ny;j++){
				njk = nx*(j+nk);
				for(i = 0;i<nx;i++){
					df[k][j][i] = (inScale*S16data[ i+njk])+inOffset;
				}
			}
		}
	}

	if ( inBitsPerPix == k16bitUIntInput ){
		U16data = (unsigned short *) d;

		for(k = 0;k<nz;k++){
			nk = ny*k;
			for(j = 0;j<ny;j++){
				njk = nx*(j+nk);
				for(i = 0;i<nx;i++){
					df[k][j][i] = (inScale*U16data[ i+njk])+inOffset;
				}
			}
		}
	}

	if ( inBitsPerPix == k32bitSIntInput ){
		S32data = (int *) d;

		for(k = 0;k<nz;k++){
			nk = ny*k;
			for(j = 0;j<ny;j++){
				njk = nx*(j+nk);
				for(i = 0;i<nx;i++){
					df[k][j][i] = (inScale*S32data[ i+njk])+inOffset;
				}
			}
		}
	}

	if ( inBitsPerPix == k32bitFltInput ){ // offset 0.0, scale = 1.0, ie simply copy
		F32data = (float *) d;

		for(k = 0;k<nz;k++){
			nk = ny*k;
			for(j = 0;j<ny;j++){
				njk = nx*(j+nk);
				for(i = 0;i<nx;i++){
					df[k][j][i] = F32data[ i+njk];
				}
			}
		}

	}

	if ( inBitsPerPix == k64bitFltInput ){ // offset 0.0, scale = 1.0, ie simply copy
		F64data = (double *) d;

		for(k = 0;k<nz;k++){
			nk = ny*k;
			for(j = 0;j<ny;j++){
				njk = nx*(j+nk);
				for(i = 0;i<nx;i++){
					df[k][j][i] = F64data[ i+njk];
				}
			}
		}

	}

	return (noErr);
}


int LoadSlicedData ( Q3DArr df, int interleave, int order, int varIndex,
                    char* fileDir, char* filePref, int level, int fileCount, char* fileSuffix);

int LoadSlicedData ( Q3DArr df, int interleave, int order, int varIndex,
                    char* fileDir, char* filePref, int level, int fileCount, char* fileSuffix){

    int err = noErr;

    if ( df != NULL ) {

        zFile  fileAccess;
        int  k,  kSampleSizeBytes;

        char    theFile[PATHLEN];

        UInt32    fileSize = 0;
        UInt32      offset = 0;

        unsigned char * U8data = NULL;
        short   * S16data = NULL;
        int     * S32data = NULL;
        double  * F64data = NULL;

        SInt32 dataSampleSize = sizeof(Real);

        switch ( inBitsPerPix ) {
            case k8bitUIntInput:
                U8data = ( unsigned char * ) inputdata;
                fileSize = (gNx*gNy);
                kSampleSizeBytes = 1L;
                break;
            case k32bitSIntInput:
            case k32bitFltInput:
                S32data = ( int * )inputdata;
                fileSize = (4L*gNx*gNy);
                kSampleSizeBytes = 4L;
                break;
            case k64bitSIntInput:
            case k64bitFltInput:
                F64data = ( double * )inputdata;
                fileSize = (8L*gNx*gNy);
                kSampleSizeBytes = 8L;
                break;
            case k16bitUIntInput:
            case k16bitSIntInput:
            default:
                S16data = ( short * )inputdata;
                kSampleSizeBytes = 2L;
                fileSize = (2L*gNx*gNy);
        }


        SInt32 inNSlices      = 1;

        if (inNSlices == 1) {

            if ( fileCount > -1) {
                sprintf(theFile,"%s%s%s%4.4d%s", fileDir, PATHSEP, filePref, fileCount, fileSuffix);
            } else {
                sprintf(theFile,"%s%s%s%s", fileDir, PATHSEP, filePref, fileSuffix);
            }
            zlsio_Printf(  " %s\n", theFile);
            switch ( inBitsPerPix ) {
                case k8bitUIntInput:


                    fileAccess = meas_open(theFile, "rb");
                    if(fileAccess != NULL) {
                        if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                        meas_read(fileAccess, U8data, fileSize*gNz);
                        meas_close(fileAccess);
                    } else {
                        zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                        return(-1);
                    }

                    break;

                case k32bitSIntInput:
                case k32bitFltInput:

                    fileAccess = meas_open(theFile, "rb");
                    if(fileAccess != NULL) {
                        if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                        meas_read(fileAccess, S32data, fileSize*gNz);
                        meas_close(fileAccess);
                    } else {
                        zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                        return(-1);
                    }

                    if ( inSwap == kByteSwapIn ) {
                        SwapData ( (void *) d, gNx,gNy,gNz);
                    }

                    break;

                case k64bitSIntInput:
                case k64bitFltInput:

                    fileAccess = meas_open(theFile, "rb");
                    if(fileAccess != NULL) {
                        if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                        meas_read(fileAccess, F64data, fileSize*gNz);
                        meas_close(fileAccess);
                    } else {
                        zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                        return(-1);
                    }

                    if ( inSwap == kByteSwapIn ) {
                        SwapData ( (void *) d, gNx,gNy,gNz);
                    }

                    break;

                case k16bitUIntInput:
                case k16bitSIntInput:
                default:

                    fileAccess = meas_open(theFile, "rb");
                    if(fileAccess != NULL) {
                        if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                        meas_read(fileAccess, S16data, fileSize*gNz);
                        meas_close(fileAccess);
                    } else {
                        zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                        return(-1);
                    }

                    if ( inSwap == kByteSwapIn ) {
                        SwapData ( (void *) d, gNx,gNy,gNz);
                    }

            } // switch




        } else { // nzSlices>1

            if ( fileCount > -1) {
                sprintf(theFile,"%s%s%s%4.4d%s/", fileDir, PATHSEP, filePref, fileCount, fileSuffix);
            } else {
                sprintf(theFile,"%s%s%s%s/", fileDir, PATHSEP, filePref, fileSuffix);
            }
            zlsio_Printf(  " %s\n", theFile);

            switch ( inBitsPerPix ) {
                case k8bitUIntInput:

                    for (k = 0; k<inNSlices; k++){
                        if ( fileCount > -1) {
                            sprintf(theFile,"%s%s%s%4.4d%s/%4.4d", fileDir, PATHSEP, filePref, fileCount, fileSuffix, k);
                        } else {
                            sprintf(theFile,"%s%s%s%s/%4.4d", fileDir, PATHSEP, filePref, fileSuffix, k);
                        }
#ifdef DEBUG
                        zlsio_Printf(  " Slice %4.4d %s\n", k,theFile );
#endif //
                        fileAccess = meas_open(theFile, "rb");
                        if(fileAccess != NULL) {
                            if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                            offset   = (gNx*gNy*gNz/inNSlices);
                            fileSize = (gNx*gNy)/inNSlices;
                            fileSize = meas_read(fileAccess, (U8data+(offset*k)), fileSize*gNz);
                            meas_close(fileAccess);
                        } else {
                            zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                            meas_close(fileAccess);
                            return(-1);
                        } // fileAccess
                    } // k

                    break;

                case k32bitSIntInput:
                case k32bitFltInput:


                    for (k = 0; k<inNSlices; k++){
                        if ( fileCount > -1) {
                            sprintf(theFile,"%s%s%s%4.4d%s/%4.4d", fileDir, PATHSEP, filePref, fileCount, fileSuffix, k);
                        } else {
                            sprintf(theFile,"%s%s%s%s/%4.4d", fileDir, PATHSEP, filePref, fileSuffix, k);
                        }
#ifdef DEBUG
                        zlsio_Printf(  " Slice %4.4d %s\n", k,theFile );
#endif //
                        fileAccess = meas_open(theFile, "rb");
                        if(fileAccess != NULL) {
                            if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                            offset   = (gNx*gNy*gNz/inNSlices);
                            fileSize = (4L*gNx*gNy)/inNSlices;
                            fileSize = meas_read(fileAccess, (S32data+(offset*k)), fileSize*gNz);
                            meas_close(fileAccess);
                        } else {
                            zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                            meas_close(fileAccess);
                            return(-1);
                        } // fileAccess
                    } // k

                    if ( inSwap == kByteSwapIn ) {
                        SwapData ( inputdata, gNx,gNy,gNz);
                    }

                    break;

                case k64bitSIntInput:
                case k64bitFltInput:


                    for (k = 0; k<inNSlices; k++){
                        if ( fileCount > -1) {
                            sprintf(theFile,"%s%s%s%4.4d%s/%4.4d", fileDir, PATHSEP, filePref, fileCount, fileSuffix, k);
                        } else {
                            sprintf(theFile,"%s%s%s%s/%4.4d", fileDir, PATHSEP, filePref, fileSuffix, k);
                        }
#ifdef DEBUG
                        zlsio_Printf(  " Slice %4.4d %s\n", k,theFile );
#endif //
                        fileAccess = meas_open(theFile, "rb");
                        if(fileAccess != NULL) {
                            if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                            offset   = (gNx*gNy*gNz/inNSlices);
                            fileSize = (8L*gNx*gNy)/inNSlices;
                            fileSize = meas_read(fileAccess, (F64data+(offset*k)), fileSize*gNz);
                            meas_close(fileAccess);
                        } else {
                            zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                            meas_close(fileAccess);
                            return(-1);
                        } // fileAccess
                    } // k

                    if ( inSwap == kByteSwapIn ) {
                        SwapData ( inputdata, gNx,gNy,gNz);
                    }

                    break;

                case k16bitUIntInput:
                case k16bitSIntInput:
                default:

                    for (k = 0; k<inNSlices; k++){
                        if ( fileCount > -1) {
                            sprintf(theFile,"%s%s%s%4.4d%s/%4.4d", fileDir, PATHSEP, filePref, fileCount, fileSuffix, k);
                        } else {
                            sprintf(theFile,"%s%s%s%s/%4.4d", fileDir, PATHSEP, filePref, fileSuffix, k);
                        }
#ifdef DEBUG
                        zlsio_Printf(  " Slice %4.4d %s\n", k,theFile );
#endif //
                        fileAccess = meas_open(theFile, "rb");
                        if(fileAccess != NULL) {
                            if (fileHeaderSkip >0) meas_seek(fileAccess, fileHeaderSkip);
                            offset   = (gNx*gNy*gNz/inNSlices);
                            fileSize = (2L*gNx*gNy)/inNSlices;
                            fileSize = meas_read(fileAccess, (S16data+(offset*k)), fileSize*gNz);
                            meas_close(fileAccess);
                        } else {
                            zlsio_Printf(  " WARNING: file not found: %s\n End.\n\n", theFile);
                            meas_close(fileAccess);
                            return(-1);
                        } // fileAccess
                    };  // k

                    if ( inSwap == kByteSwapIn ) {
                        SwapData ( inputdata, gNx, gNy, gNz);
                    }

            } // switch

        }

        ScaleData( df, (void *)inputdata, gNx, gNy, gNz );

            //======================================================================================
            //
            // operate on zone before putting into data
            //
            //======================================================================================

        switch (inTransform) {
            default:
            case kLinearTransIn:
                ;
                break;
            case kLogTransIn:
                logZone(df, gNx, gNy, gNz , dataSampleSize );
                break;
            case kExpTransIn:
                expZone(df, gNx, gNy, gNz , dataSampleSize );
                break;
            case kSqrTransIn:
                sqrZone(df, gNx, gNy, gNz , dataSampleSize );
                break;
            case kSqrtTransIn:
                sqrtZone(df, gNx, gNy, gNz , dataSampleSize );
        }

        if ( inScaleTransform  == kScaleTransIn ){
            scaleZone(inScaleFactor, df, gNx, gNy, gNz , dataSampleSize );
        }
        if ( inPowerTransform  == kPowTransIn ){
            powZone(inPowerFactor, df, gNx, gNy, gNz , dataSampleSize );
        }

    } // df

    return err;

}

    // get data at varIndex in single or compound bdat file, varIndex is 0 based index 0--(n-1)
int LoadZLSData ( Q3DArr df, int interleave, int order, int varIndex,
                 char* fileDir, char* filePref, int level, int fileCount, char* fileSuffix);
int LoadZLSData ( Q3DArr df, int interleave, int order, int varIndex,
                 char* fileDir, char* filePref, int level, int fileCount, char* fileSuffix){

    int err = noErr;

    zFile  fileAccess = NULL;
    int  k;

    char theFile[PATHLEN];
    char varCode[6] = "     ";

    UInt32 fileSize = 0;

    UInt32 zoneDataOffset = ZLS_DATA_HEADERSIZE;
    UInt32 fileOffset     = 0L;

    UInt32 zoneSample     = k32bitFltSize ;
    UInt32 zoneFormat     = k32bitFltInput ;
    UInt32 zoneSwap       = kNoByteSwapIn ;

    SInt32 zoneDataMax    = 32000;
    SInt32 zoneDataMin    = 0;

    double zoneDataMaxValue = 1.0 ;
    double zoneDataMinValue = 0.0 ;

    UInt32 zoneNX = 1;
    UInt32 zoneNY = 1;
    UInt32 zoneNZ = 1;

    SInt32 zoneNX0 = 0;
    SInt32 zoneNY0 = 0;
    SInt32 zoneNZ0 = 0;

#ifdef DEBUG
    zlsio_Printf(  " ------------------------------------------------------\n");
    zlsio_Printf(  " Loading ZLS Data: %s%4.4d\n", dataPref, fileCount);
#endif // DEBUG


    for (k = 0; k<nzZones; k++){
        if ((k % interleave) == order ) {

            if ( nzZones == 1) {

                if ( fileCount > -1) {
                    sprintf(theFile,"%s%s%sL%2.2dC%4.4d%s", fileDir, PATHSEP, filePref, level, fileCount, fileSuffix);
                } else {
                    sprintf(theFile,"%s%s%s%s", fileDir, PATHSEP, filePref, fileSuffix);
                }

#ifdef DEBUG
                zlsio_Printf(  " %s\n", theFile);
#endif // DEBUG

            } else {

                if ( fileCount > -1) {
#ifdef ZLSDATAVERSION6
                    sprintf( theFile, "%s%sC%4.4d%s%sL%2.2dZ%2.2dC%4.4d%s", fileDir, PATHSEP, fileCount, PATHSEP, filePref, level,   k, fileCount, fileSuffix);
#else
                    sprintf( theFile, "%s%sC%4.4d%s%sL%2.2dZ%4.4dC%4.4d%s", fileDir, PATHSEP, fileCount, PATHSEP, filePref, level,   k, fileCount, fileSuffix);
#endif
                } else {
                    sprintf(theFile,"%s%s%s%s", fileDir, PATHSEP, filePref, fileSuffix);
                }
            }

            if ( (k % 256) == 0 ) zlsio_Printf(" Data File: %s\n", theFile);


            fileAccess = meas_open(theFile, "rb");
            if (  fileAccess == NULL ) {
                int err = errno;
                zlsio_Printf(" Data File Error : %d \n", err );
                return err;
            }

            err = GetZLSDataInfo (fileAccess, varIndex, varCode, &zoneNX, &zoneNY, &zoneNZ, &zoneSwap,
                                  &zoneNX0, &zoneNY0, &zoneNZ0, &zoneSample, &zoneFormat,
                                  &zoneDataMax, &zoneDataMin, &zoneDataMaxValue, &zoneDataMinValue,
                                  &zoneDataOffset, &fileOffset);

                // meas_close(fileAccess);

            if ( (k % 256) == 0 ) {
                zlsio_Printf(" Data : %s Index: %d Offset: %d Err: %d \n", varCode, varIndex, zoneDataOffset, err );
            }

#ifdef DEBUG
            zlsio_Printf(  " Zone %4.4d %s\n", k, theFile );
            zlsio_Printf(  "    Expected Header Size %ld, Found Header Size %d\n", ZLS_DATA_HEADERSIZE,zoneDataOffset );
            zlsio_Printf(  "    Data Dimensions: %d %d %d\n", zoneNX,zoneNY,zoneNZ );
            zlsio_Printf(  "    Data Location  : %d %d %d\n", zoneNX0,zoneNY0,zoneNZ0 );

            switch (zoneFormat) {
                case k8bitUIntInput:
                    zlsio_Printf(  "    Format: 8 bit unsigned bytes \n"); break;
                case k8bitSIntInput:
                    zlsio_Printf(  "    Format: 8 bit signed bytes \n"); break;
                case k16bitUIntInput:
                    zlsio_Printf(  "    Format: 16 bit unsigned short ints \n"); break;
                case k16bitSIntInput:
                    zlsio_Printf(  "    Format: 16 bit unsigned short ints \n"); break;
                case k32bitUIntInput:
                    zlsio_Printf(  "    Format: 32 bit unsigned ints \n"); break;
                case k32bitSIntInput:
                    zlsio_Printf(  "    Format: 32 bit signed ints \n"); break;
                case k32bitFltInput:
                    zlsio_Printf(  "    Format: 32 bit floats \n"); break;
                case k64bitUIntInput:
                    zlsio_Printf(  "    Format: 64 bit unsigned long ints \n"); break;
                case k64bitSIntInput:
                    zlsio_Printf(  "    Format: 64 bit signed long ints \n"); break;
                case k64bitFltInput:
                    zlsio_Printf(  "    Format: 64 bit doubles \n"); break;
                default:
                    zlsio_Printf(  "    Format: Unknown! \n");
            }
#endif // DEBUG

            if ( err >= 0 ) {

                zoneScale   = 1.0;

#ifdef DEBUG
                zlsio_Printf( " Max Value %#10.5g Min Value %#10.5g\n", zoneDataMaxValue, zoneDataMinValue );
                zlsio_Printf( " ZLS Data1: \n %d %d %d %d \n", zoneNX,zoneNY,zoneNZ, zoneSwap);
                zlsio_Printf( " ZLS Data2: \n %d %d %d %d \n", zoneNX0, zoneNY0, zoneNZ0, zoneFormat);
                zlsio_Printf( " ZLS Data3: \n %d %d %g %g \n", zoneDataMax, zoneDataMin, zoneDataMaxValue, zoneDataMinValue);
                zlsio_Printf( " ZLS Data4: \n %g %d %s \n", zoneScale, zoneDataOffset, theFile);
#endif // DEBUG

                inSwap = zoneSwap;

                    // with multiple zones, each may or may not have independent integer scaling
                    // keep track of overall max and min floating point values
                unsigned char * zdata = NULL;
                    // float * zfdata = NULL;

                fileSize = zoneSample*(zoneNX*zoneNY*zoneNZ); // bytes
                zdata  = (unsigned char *) malloc( fileSize);

                if ( (zdata != NULL) ) {

                    meas_read(fileAccess, zdata, fileSize);

                    if ( ( inSwap == kByteSwapIn ) && (zoneSample > 1) ){
#ifdef DEBUG
                        zlsio_Printf( " ZLS Data: swapping, %d %d %d %d \n", zoneNX, zoneNY, zoneNZ, zoneFormat);
#endif // DEBUG
                        SwapDataSize( (void *) zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                    }


                        //======================================================================================
                        //
                        // operate on zone before putting into data
                        //
                        //======================================================================================
                    switch (inTransform) {
                        default:
                        case kLinearTransIn:
                            ;
                            break;
                        case kLogTransIn:
                            logZone(zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                            break;
                        case kExpTransIn:
                            expZone(zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                            break;
                        case kSqrTransIn:
                            sqrZone(zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                            break;
                        case kSqrtTransIn:
                            sqrtZone(zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                    }

                    if ( inScaleTransform  == kScaleTransIn ){
                        scaleZone(inScaleFactor, zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                    }
                    if ( inPowerTransform  == kPowTransIn ){
                        powZone(inPowerFactor, zdata, zoneNX, zoneNY, zoneNZ, zoneSample);
                    }

                    switch (inOperation) {
                        default:
                        case kLoadOperationIn:
                            CopyZoneIntoData( df,     gNx,     gNy,    gNz,
                                             zdata,  zoneNX,  zoneNY, zoneNZ,
                                             zoneNX0, zoneNY0, zoneNZ0, zoneSample);
                            break;
                        case kAddOperationIn:
                            AddZoneIntoData( df,     gNx,     gNy,    gNz,
                                            zdata,  zoneNX,  zoneNY, zoneNZ,
                                            zoneNX0, zoneNY0, zoneNZ0, zoneSample);
                            break;
                        case kMulOperationIn:
                            MultiplyZoneIntoData( df,     gNx,     gNy,    gNz,
                                                 zdata,  zoneNX,  zoneNY, zoneNZ,
                                                 zoneNX0, zoneNY0, zoneNZ0, zoneSample);
                            break;
                    }// switch

                    if (zdata != NULL)  free (zdata);    zdata = NULL;

                } // zdata

            } // get info OK

            meas_close(fileAccess);
        }
    } // k

    return (err);

}

#ifdef PTHREADS

int ProcessZLSLoadData( int interleave, int order);
int ProcessZLSLoadData( int interleave, int order){
    int err = noErr;
      //zlsio_Printf(  " ------------------------------------------------------\n");
      //zlsio_Printf(  " Loading ZLS Data: %s%4.4d\n", dataPref, fileLoadIndex);
      //globals don't change for sub zones, fileLoadIndex is constant until all zones loaded
    err = LoadZLSData( d, interleave, order, dataVarIndex,
                      dataDir, dataPref, zlsLevel, fileLoadIndex, dataSuff);

    if(err != noErr) {
        zlsio_Printf(  " ERROR: ProcessZLSLoadData file: %s%4.4d\n", dataPref, fileLoadIndex);
    }

    return err;
}

int     QueuePThreadZLSLoadData ( int pthreadID );
void *  PThreadLoadZLSData ( void * pthreadID );

int QueuePThreadZLSLoadData ( int pthreadID ) {

    int err = noErr;

    pthread_t    thread;
    pthread_attr_t attr;

    err = pthread_attr_init(&attr);
    err |= pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    err |= pthread_create(&thread, &attr, PThreadLoadZLSData, (void *) &gLoadProcessIDs[pthreadID]);
    err |= pthread_attr_destroy(&attr);

    return(err);
}

void *  PThreadLoadZLSData  ( void * pthreadID ) {

    int * id = (int *) pthreadID;
    int interleave = NTHREADS;
    int order = * id;

    pthread_mutex_lock            ( &gSweepLoadMutex);
    if (gSweepLoadLock >0) pthread_cond_broadcast  ( &gNextSweepLoadCond );
        // first time in, sit and wait for gSweepCond launches
    pthread_cond_wait             ( &gNextSweepLoadCond, &gSweepLoadMutex);
    pthread_mutex_unlock          ( &gSweepLoadMutex);

    ProcessZLSLoadData( interleave, order );

    pthread_mutex_lock             ( &gSweepLoadMutex);
#if (ZLS_DEBUG > 0)
    printf( " *** Sweep %d done. Cycle: %d \n",(int)z->zoneID, (int)z->zoneClock.cycles );
#endif //
    gSweepLoadLock--;     // if gSweepLock hits 0 then it is all over
    pthread_cond_broadcast  ( &gNextSweepLoadCond ); // request next or end of all sweeps (gSweepLock hits 0)
    pthread_mutex_unlock    ( &gSweepLoadMutex);
    pthread_exit(NULL); // thread done

}
    //====================================================================================

    //====================================================================================
int WaitPThreadLoadZLSData ( int nThreads ); // conduct sweeper threads
int WaitPThreadLoadZLSData ( int nThreads ){ // conduct sweeper threads
    int err = noErr;
    err = pthread_mutex_lock       ( &gSweepLoadMutex );
    gSweepLoadLock = nThreads;
    while ( gSweepLoadLock > 0 ) {
        err |= pthread_cond_broadcast ( &gNextSweepLoadCond );
        err |= pthread_cond_wait      ( &gNextSweepLoadCond, &gSweepLoadMutex );
    }
#if (ZLS_DEBUG > 0)
    printf( " >>>> %d Thread Sweeps Over\n", nThreads );
#endif //
    gSweepLoadLock = 0;
    err = pthread_mutex_unlock     ( &gSweepLoadMutex );
    return(err);
}
    //====================================================================================

#endif // PTHREADS

int ReadData(int fileCount){

    int    err = 0;

    int idx;
    fileLoadIndex = fileCount;

    inputdata = NULL;

    if (inFormatType == kRawInput) {
        switch ( inBitsPerPix ) {
            case k8bitUIntInput:
                inputdata = malloc(gNx*gNy*gNz);
                break;
            case k32bitSIntInput:
            case k32bitFltInput:
                inputdata = malloc(4L*gNx*gNy*gNz);
                break;
            case k64bitSIntInput:
            case k64bitFltInput:
                inputdata = malloc(8L*gNx*gNy*gNz);
                break;
            case k16bitUIntInput:
            case k16bitSIntInput:
            default:
                inputdata = malloc(2L*gNx*gNy*gNz);
        }
    }

    for ( idx = 0 ; idx < nVars; idx ++) {

        switch (idx) {
            default:
            case 0:
                sprintf(dataDir, "%s%s%s",dataBaseDir, PATHSEP,dataDir0);
                strcpy(dataPref, dataPref0);
                strcpy(dataSuff, dataSuff0);
                dataVarIndex     = dataVarIndex0;
                inOperation      = inOperation0;
                inTransform      = inTransform0;
                inScaleTransform = inScaleTransform0;
                inPowerTransform = inPowerTransform0;
                inScaleFactor = inScaleFactor0;
                inPowerFactor = inPowerFactor0;
                d        = data0; //P
                break;

            case 1:
                sprintf(dataDir, "%s%s%s",dataBaseDir, PATHSEP,dataDir1);
                strcpy(dataPref, dataPref1);
                strcpy(dataSuff, dataSuff1);
                dataVarIndex     = dataVarIndex1;
                inOperation      = inOperation1;
                inTransform      = inTransform1;
                inScaleTransform = inScaleTransform1;
                inPowerTransform = inPowerTransform1;
                inScaleFactor = inScaleFactor1;
                inPowerFactor = inPowerFactor1;
                d        = data1; //d
                break;

            case 2:
                sprintf(dataDir, "%s%s%s",dataBaseDir, PATHSEP,dataDir2);
                strcpy(dataPref, dataPref2);
                strcpy(dataSuff, dataSuff2);
                dataVarIndex     = dataVarIndex2;
                inOperation      = inOperation2;
                inTransform      = inTransform2;
                inScaleTransform = inScaleTransform2;
                inPowerTransform = inPowerTransform2;
                inScaleFactor = inScaleFactor2;
                inPowerFactor = inPowerFactor2;
                d        =  data2; //velm
                break;

            case 3:
                sprintf(dataDir, "%s%s%s",dataBaseDir, PATHSEP,dataDir3);
                strcpy(dataPref, dataPref3);
                strcpy(dataSuff, dataSuff3);
                dataVarIndex     = dataVarIndex3;
                inOperation      = inOperation3;
                inTransform      = inTransform3;
                inScaleTransform = inScaleTransform3;
                inPowerTransform = inPowerTransform3;
                inScaleFactor = inScaleFactor3;
                inPowerFactor = inPowerFactor3;
                d        =  data3; //PB = 0.5B^2
                break;

            case 4:
                sprintf(dataDir, "%s%s%s",dataBaseDir, PATHSEP,dataDir4);
                strcpy(dataPref, dataPref4);
                strcpy(dataSuff, dataSuff4);
                dataVarIndex     = dataVarIndex4;
                inOperation      = inOperation4;
                inTransform      = inTransform4;
                inScaleTransform = inScaleTransform4;
                inPowerTransform = inPowerTransform4;
                inScaleFactor = inScaleFactor4;
                inPowerFactor = inPowerFactor4;
                d        =  data2; //vz2
                break;
        }

        if ( d != NULL) {
                // operation to combine input vars
            zlsio_Printf(" Loading Data %d : %s ... %s index: %d\n", idx, dataPref, dataSuff, dataVarIndex);

            switch (inTransform) {
                default:
                case kLinearTransIn:
                    zlsio_Printf(" Load Transform : None\n");
                    break;
                case kLogTransIn:
                    zlsio_Printf(" Load Transform : Log10(x)\n");
                    break;
                case kExpTransIn:
                    zlsio_Printf(" Load Transform : 10^x\n");
                    break;
                case kSqrTransIn:
                    zlsio_Printf(" Load Transform : x^2\n");
                    break;
                case kSqrtTransIn:
                    zlsio_Printf(" Load Transform : Sqrt(x)\n");
                    break;
            }
            
            if (inScaleTransform == kScaleTransIn) {
                zlsio_Printf(" Load Scaling Factor: f*x : %g\n",inScaleFactor );
            }
            
            if (inPowerTransform == kPowTransIn) {
                zlsio_Printf(" Load Power Factor  : x^f : %g\n", inPowerFactor);
            }
            
            
            switch (inOperation) {
                default:
                case kLoadOperationIn:
                    zlsio_Printf(" Load Operation : Copy zone/s into data.\n");
                    break;
                case kAddOperationIn:
                    zlsio_Printf(" Load Operation : Add zone/s into data.\n");
                    break;
                case kMulOperationIn:
                    zlsio_Printf(" Load Operation : Multiply zone/s into data.\n");
                    break;
            }
            
#ifdef PTHREADS
            
            if (inFormatType == kRawInput) {
                zlsio_Printf(  " ERROR: Loading file: Input type Raw/Sliced data not compatible with PTHREADS\n");
                return(-1);
            }
            
            gSweepLoadLock =0;	// global thread counting mutex variable.
            
            int pid;
            for ( pid=0; pid < NTHREADS; pid++) {
                gLoadProcessIDs[pid] = pid; // store id in static global so it is available later when the thread actually starts
                QueuePThreadZLSLoadData  ( pid );
            }
            WaitPThreadLoadZLSData( NTHREADS ); // wait threads to finish
            
            if(err != noErr) {
                zlsio_Printf(  " ERROR: Loading file: %s%s%s%4.4d\n End.\n\n", dataDir,PATHSEP, dataPref, fileCount);
                return(-1);
            }
#else
            int interleave = 1; // every one of nSub-Zones
            int order = 0; // order within interleave group, 0 = first
            
            if (inFormatType == kRawInput) {
                err = LoadSlicedData( d, interleave, order, dataVarIndex,
                                     dataDir, dataPref, zlsLevel, fileLoadIndex, dataSuff);
            } else {
                err = LoadZLSData( d, interleave, order, dataVarIndex,
                                  dataDir, dataPref, zlsLevel, fileLoadIndex, dataSuff);
            }
            
            if(err != noErr) {
                zlsio_Printf(  " ERROR: Loading file: %s%s%s%4.4d\n End.\n\n", dataDir,PATHSEP, dataPref, fileCount);
                return(-1);
            }
#endif
            
            
            zlsio_Printf(  " Read File: %s%4.4d%s \n At: %s",
                         dataPref, fileLoadIndex, dataSuff, TheDateAndTime());
            zlsio_Printf(  " ------------------------------------------------------\n");
            
        }
        
	}// idx
    /*
     if (data1 != NULL) zls_DisposeQuantity3DArr(data1); data1 = NULL;
     if (data2 != NULL) zls_DisposeQuantity3DArr(data2); data2 = NULL;
     if (data3 != NULL) zls_DisposeQuantity3DArr(data3); data3 = NULL;
     if (data4 != NULL) zls_DisposeQuantity3DArr(data4); data4 = NULL;
     */ 
        //if (zfdata != NULL) free (zfdata);  zfdata = NULL;
        //  if (zdata != NULL)  free (zdata);    zdata = NULL;
    
    if (inFormatType == kRawInput) {
        if ( inputdata != NULL ) free(inputdata); inputdata = NULL;
    }
    
	return(err);
	
}

