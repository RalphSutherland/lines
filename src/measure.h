#ifndef MEASURE_H
#define MEASURE_H
#include "options.def"

//#include <stdlib.h>
//#include <ctype.h>
//#include <stdio.h>
//#include <math.h>
//#include <limits.h>
//#include <ctype.h>
//#include <unistd.h>
//#include <stdarg.h>
//#include <sys/stat.h>
//#include <errno.h>
//#include <sys/types.h>
//#include <sys/times.h>
//#include <time.h>
//
//#include <limits.h>
//#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <limits.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/times.h>
#include <time.h>

#ifdef PTHREADS
#include  <pthread.h>
#endif // PTHREADS

#include <fftw3.h>

#include "zlib.h"
#include "png.h"
#include "ccl.h"

#ifdef ZLIB_COMPRESSED_IO
typedef gzFile zFile;
#else
typedef FILE * zFile;
#endif // !ZLIB_COMPRESSED_IO

#include "const.h"

#include "zls_arrs.h"
#include "zls_qs.h"
#include "zls_fitpoly.h"
#include "zls_util.h"
#include "zls_log.h"

#include "user.h"

#ifdef MPI
#include  <mpi.h>
extern int  gMPIrank;
extern int  gMPIsize;
extern MPI_Status  gMPIstatus;
#endif // MPI

//------------------------
//
// globals
//
//------------------------
//
// array sizes
//  
//------------------------

extern  SInt32 nzZones;
extern  SInt32 zlsLevel;

extern  SInt32 file0;
extern  SInt32 file1;

// Output Format/transform

extern  SInt32 ioCompressed;

// Input Format/transform

extern SInt32 inBitsPerPix ;
extern SInt32 inFormatType ;
extern SInt32 inFormat     ;
extern SInt32 inSwap       ;
extern Real   inScale      ;
extern Real   inOffset     ;

extern SInt32 inTransform;
extern SInt32 inOperation;

extern SInt32 inScaleTransform;
extern Real inScaleFactor;

extern SInt32 inPowerTransform;
extern Real inPowerFactor;
  
// Input
extern Real fileMax;
extern Real fileMin;
extern size_t fileHeaderSkip; 
extern Real dataMax;
extern Real dataMin;
extern Real dataThr;
extern Real dataLim;
extern Real dataNullLim;
extern Real dataNullThr;
extern Real dataScale;

extern Real zoneScale;
extern Real zoneMin;
extern Real zoneMax;

extern Real gridScale  ; // given,  default = 1.0, ususally set to size of whole grid
extern Real lengthScale; // given,  default = 1.0, physical scaling
extern Real areaScale  ; // default = 1.0, usually lengthScale^3 :)
extern Real volumeScale; // default = 1.0, usually lengthScale^3 :)
extern Real timeScale  ; // length/velocity
extern Real velocityScale ; // given physical scaling
extern Real accellScale   ;   // vel/t
extern Real densityScale  ;  //given,  physical scale ie rho or P or E
extern Real pressureScale ;  // derived  
extern Real emissionScale ;  // derived,  used for RGB X-ray rendering,ignored otherwise

extern Real cellLength ; // for integrating by summing cells
extern Real cellArea   ; // for integrating by summing cells
extern Real cellVolume ; // for integrating by summing cells

extern Real rayleighUnits ; // for integrating H Alpha surface brightnesses
extern Real kGamma;

extern  SInt32 nVars;

extern  char dataBaseDir [DIRLEN];

extern  char dataDir [DIRLEN];
extern  char dataPref[80];
extern  char dataSuff[16];
extern  SInt32 dataVarIndex;

extern  char dataDir0 [DIRLEN];
extern  char dataPref0[80];
extern  char dataSuff0[16];
extern  SInt32 dataVarIndex0;

extern  char dataDir1 [DIRLEN];
extern  char dataPref1[80];
extern  char dataSuff1[16];
extern  SInt32 dataVarIndex1;

extern  char dataDir2 [DIRLEN];
extern  char dataPref2[80];
extern  char dataSuff2[16];
extern  SInt32 dataVarIndex2;

extern  char dataDir3 [DIRLEN];
extern  char dataPref3[80];
extern  char dataSuff3[16];
extern  SInt32 dataVarIndex3;

extern  char dataDir4 [DIRLEN];
extern  char dataPref4[80];
extern  char dataSuff4[16];
extern  SInt32 dataVarIndex4;

extern  Q3DArr    d;  // Real32/64 cube of current data

extern  Q3DArr data0;  // Real32/64 cube to measure
extern  Q3DArr data1;  // Real32/64 cube to measure
extern  Q3DArr data2;  // Real32/64 cube to measure
extern  Q3DArr data3;  // Real32/64 cube to measure
extern  Q3DArr data4;  // Real32/64 cube to measure

extern  SInt32 inOperation0      ;
extern  SInt32 inTransform0      ;
extern  SInt32 inScaleTransform0 ;
extern  Real   inScaleFactor0    ;
extern  SInt32 inPowerTransform0 ;
extern  Real   inPowerFactor0    ;

extern  SInt32 inOperation1      ;
extern  SInt32 inTransform1      ;
extern  SInt32 inScaleTransform1 ;
extern  Real   inScaleFactor1    ;
extern  SInt32 inPowerTransform1 ;
extern  Real   inPowerFactor1    ;

extern  SInt32 inOperation2      ;
extern  SInt32 inTransform2      ;
extern  SInt32 inScaleTransform2 ;
extern  Real   inScaleFactor2    ;
extern  SInt32 inPowerTransform2 ;
extern  Real   inPowerFactor2    ;

extern  SInt32 inOperation3      ;
extern  SInt32 inTransform3      ;
extern  SInt32 inScaleTransform3 ;
extern  Real   inScaleFactor3    ;
extern  SInt32 inPowerTransform3 ;
extern  Real   inPowerFactor3    ;

extern  SInt32 inOperation4      ;
extern  SInt32 inTransform4      ;
extern  SInt32 inScaleTransform4 ;
extern  Real   inScaleFactor4    ;
extern  SInt32 inPowerTransform4 ;
extern  Real   inPowerFactor4    ;

#define PATHSEP "/"

//
// TIFF Structures
//

typedef struct {
    unsigned short tCode;
    unsigned short dataType;
    unsigned int   nDataValues;
    unsigned int   pDataField;
} TagStructure;

// TIFF Colour table

typedef struct{
    unsigned short red[256];   // 0x0000 -> 0xFFFF, if input is 0-255 then byte double
    unsigned short green[256]; // 0x0000 -> 0xFFFF, ie 0xCA -> 0xCACA
    unsigned short blue[256];  // 0x0000 -> 0xFFFF
} TIFFCTab;

enum { kRAWOutput   = 0x00,
    kTIFFOutput  = 0x01,
    kJPEGOutput  = 0x02,
    kPNGOutput   = 0x03,
    k8bitOutput  = 0x08,  // I know ! but it is more direct.
    k16bitOutput = 0x10,
    kS16bitOutput= 0x11,
    kGZIPOutput  = 0x01
};

extern SInt32  outFormat;

extern  char outName[PATHLEN];
extern  char fileName[PATHLEN];
extern  char outLogFileName[PATHLEN];
extern  zFile outLogFILE;

extern char outDir [DIRLEN];
extern char outPref[80];
extern char outSuff[16];

extern char outCTABFile[PATHLEN];
extern TIFFCTab outTIFFCTab;

extern float * image32;

#include "protos.h"

#endif //
