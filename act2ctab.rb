#!/usr/bin/ruby
#
# convert adobe PS .act colour tables to UTF-8 .ctabs
#

require 'base64'

base_file  =  ARGV[0]
n_bytes  = 256
rgb_array  = Array.new(n_bytes*3)

File.open(base_file, "rb") do |file|
  rgb_array   = file.read(n_bytes*3).bytes
end

puts "1"
puts "8"
puts "3"
puts "#{n_bytes}"

n_bytes.times do |i|
  printf("%3.3d %2.2x %2.2x %2.2x \n", i, rgb_array[(i*3)], rgb_array[(i*3)+1], rgb_array[(i*3)+2])
end

