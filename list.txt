######################################################
# U-Band spectrum including Oxygen II
######################################################
#
# single H line patch, auto continuum
#
Patch_001_Line    = 3750.0:0.7
#
# single H line patch, manual continuum
#
Patch_002_Left  = 3688.5:3689.5
Patch_002_Right = 3693.0:3695.5
Patch_002_Line  = 3691.5:0.7
#
# two line patch: [OII] doublet + Hlines , manual continuum
#
Patch_003_Left    = 3714:3720
Patch_003_Right   = 3736.5:3742
Patch_003_Fixed_Widths  = 0
Patch_003_nLines  = 4
Patch_003_Line_01 = 3722.0:0.7
Patch_003_Line_02 = 3726.0:0.7
Patch_003_Line_03 = 3729.0:0.7
Patch_003_Line_04 = 3734.0:0.7
#
######################################################
