#include "measure.h"

#ifdef BIGENDIAN
#define TIFF_BIGENDIAN_OS
#endif

#ifdef LITENDIAN
#define TIFF_LITENDIAN_OS
#endif


static inline int ImageMaxMin(  float * img, float * max, float * min, unsigned int nx, unsigned int ny) ;
static inline int ImageMaxMin(  float * img, float * max, float * min, unsigned int nx, unsigned int ny) {
      // find max and min > 0 for image
      float maxV = img[0];
      float minV = img[0];
      unsigned int nij = nx*ny;
      int      i;

      for(i = 0;i<nij;i++){
            maxV = maxV < img[i] ? img[i] :maxV;
      }

      for(i = 0;i<nij;i++){
            minV = minV < img[i] ? minV: img[i] ;
      }

      *max = *max < maxV ? maxV : *max;
      *min = *min < minV ? *min: minV ;

      return(0);
}

int ScaleAndSaveImage ( char * theFile, float * img, SInt32 nx, SInt32 ny ){

      int i, j, v, jRev;
      float max, min, range, f;

      // single channel output

      unsigned char *  image8 = ( unsigned char * ) malloc(sizeof(unsigned char)*nx*ny);

      if ( image8 != NULL ) {

            max = -FLT_MAX;
            min =  FLT_MAX;

            ImageMaxMin( img, &max, &min, nx, ny );

            range = max - min;

            f     = 253.0F/range;

            zlsio_Printf(" Max Min Range and Scale %g %g %g %g\n",max, min, range, f);

            for(j = 0;j<ny;j++){
                  jRev = ny-j-1;
                  for(i = 0;i<nx;i++){

                        v = f*(img[i + nx*j]-min)+1;

                        v = v<1?1:v;  // reserve B & W in 0 and 255 of CT
                        image8[i + nx*jRev] = v >0xFE?0xFE:v;

                        img[i + nx*j]   = 0.0F; // clear as we go, ready for next loop

                  } //i
            } //j

            if (outFormat == kRAWOutput){

                  Write_raw8(theFile, image8, nx, ny );

            }

            if (outFormat == kTIFFOutput){

                  Write_tif8(theFile, image8, &outTIFFCTab, nx, ny );

            }

            if ( (outFormat == kJPEGOutput)||
                (outFormat == kPNGOutput )  ) {

                  Write_png8(theFile, image8, &outTIFFCTab, nx, ny );

            }

            free((void *) image8);

      } // image8

      return(0);

}


static inline int Image64MaxMin(  double * img, double * max, double * min, unsigned int nx, unsigned int ny) ;
static inline int Image64MaxMin(  double * img, double * max, double * min, unsigned int nx, unsigned int ny) {

      // find max and min > 0 for image
      double maxV = img[0];
      double minV = img[0];
      unsigned int nij = nx*ny;
      int      i;

      for(i = 0;i<nij;i++){
            maxV = maxV < img[i] ? img[i] :maxV;
      }

      for(i = 0;i<nij;i++){
            minV = minV < img[i] ? minV: img[i] ;
      }

      *max = *max < maxV ? maxV : *max;
      *min = *min < minV ? *min: minV ;

      return(0);

}

int ScaleAndSaveImage64 ( char * theFile, double * img, SInt32 nx, SInt32 ny ){

      int i, j, v, jRev;
      double max, min, range, f;

      // single channel output

      unsigned char *  image8 = ( unsigned char * ) malloc(sizeof(unsigned char)*nx*ny);

      if ( image8 != NULL ) {

            max = img[0];
            min = img[0];

            Image64MaxMin( img, &max, &min, nx, ny );

            range = max - min;

            f     = 253.0F/range;

            zlsio_Printf(" Max Min Range and Scale %g %g %g %g\n",max, min, range, f);

            for(j = 0;j<ny;j++){
                  jRev = ny-j-1;
                  for(i = 0;i<nx;i++){

                        v = f*(img[i + nx*j]-min)+1;

                        v = v<1?1:v;  // reserve B & W in 0 and 255 of CT
                        image8[i + nx*jRev] = v >0xFE?0xFE:v;

                        img[i + nx*j]   = 0.0F; // clear as we go, ready for next loop

                  } //i
            } //j

            if (outFormat == kRAWOutput){

                  Write_raw8(theFile, image8, nx, ny );

            }

            if (outFormat == kTIFFOutput){

                  Write_tif8(theFile, image8, &outTIFFCTab, nx, ny );

            }

            if ( (outFormat == kJPEGOutput)||
                (outFormat == kPNGOutput )  ) {

                  Write_png8(theFile, image8, &outTIFFCTab, nx, ny );

            }

            free((void *) image8);

      } // image8

      return(0);

}

static inline int ImageLogMaxMin(  float * img, float * max, float * min, unsigned int nx, unsigned int ny) ;
static inline int ImageLogMaxMin(  float * img, float * max, float * min, unsigned int nx, unsigned int ny) {
      // find max and min > 0 for image
      float maxV = -FLT_MAX;
      float minV =  FLT_MAX;
      unsigned int nij = nx*ny;
      int      i;

      for(i = 0;i<nij;i++){
            Real32 val = img[i];
            maxV = maxV < val ? val :maxV;
            minV = (val>0.0)?((minV>val)?val:minV):minV;
      }
      *max = *max < maxV ? maxV : *max;
      *min = *min < minV ? *min: minV ;
      return(0);
}


int ScaleAndSaveLogImage ( char * theFile, float * img, SInt32 nx, SInt32 ny ){

      int i, j, v, jRev;
      Real32 max, min;

      // single channel output

      unsigned char *  image8 = ( unsigned char * ) malloc(sizeof(unsigned char)*nx*ny);

      if ( image8 != NULL ) {

            max = -FLT_MAX;
            min =  FLT_MAX;

            ImageLogMaxMin( img, &max, &min, nx, ny );

            Real32 lmn   = log10f(min);
            Real32 lmx   = log10f(max);
            Real32 range = lmx-lmn;
            Real32 f    = 253.0F/range;

            zlsio_Printf(" Log Max Min Range and Scale %g %g %g %g\n",lmx, lmn, range, f);

            for(j = 0;j<ny;j++){
                  jRev = ny-j-1;
                  for(i = 0;i<nx;i++){
                        Real32 val = img[i + nx*j];
                        v = f*(log10f((val>0.0f)?val:min)-lmn)+1.0F;
                        v = v<1?1:v;  // reserve B & W in 0 and 255 of CT
                        image8[i + nx*jRev] = v >0xFE?0xFE:v;
                        img[i + nx*j]   = 0.0F; // clear as we go, ready for next loop
                  } //i
            } //j

            if (outFormat == kRAWOutput){
                  Write_raw8(theFile, image8, nx, ny );
            }

            if (outFormat == kTIFFOutput){
                  Write_tif8(theFile, image8, &outTIFFCTab, nx, ny );
            }

            if ( (outFormat == kJPEGOutput)||
                (outFormat == kPNGOutput )  ) {
                  Write_png8(theFile, image8, &outTIFFCTab, nx, ny );
            }

            free((void *) image8);

      } // image8

      return(0);

}


//----------------------------------------------------------------------------------------------------------------
//
// Raw File IO Routines
//
//----------------------------------------------------------------------------------------------------------------


int Write_rawF32     ( char * aFile, Real32 * d, int nxOut, int nyOut  ){

      int err = noErr;
      zFile fileAccess = NULL;  size_t fileSize= 0;

      fileAccess = meas_open( aFile, "wb");
      if ( fileAccess ) {
            fileSize = (4L*nxOut*nyOut);
            err      = meas_write( fileAccess, d, fileSize );
            meas_close( fileAccess );
      } else { err = -1; }

      return (err);

}

int Write_rawF64     ( char * aFile, Real * d, int nxOut, int nyOut  ){

      int err = noErr;
      zFile fileAccess = NULL;  size_t fileSize= 0;

      fileAccess = meas_open( aFile, "wb");
      if ( fileAccess ) {
            fileSize = (8L*nxOut*nyOut);
            err      = meas_write( fileAccess, d, fileSize );
            meas_close( fileAccess );
      } else { err = -1; }

      return (err);

}


int Write_raw8     ( char * aFile, unsigned char * img, int nxOut, int nyOut  ){
      int err = noErr;
      zFile fileAccess = NULL;  size_t  fileSize= 0;

      fileAccess = meas_open( aFile, "wb");
      if (fileAccess) {
            fileSize = (nxOut*nyOut);
            err      =meas_write( fileAccess, img, fileSize );
            meas_close(fileAccess);
      } else { err = -1; }
      return (err);
}


//----------------------------------------------------------------------------------------------------------------
//
// TIFF File IO Routines
//
//----------------------------------------------------------------------------------------------------------------


int Write_tif8     (char * aFile, unsigned char * img, TIFFCTab * outTIFFCTab, int nxOut, int nyOut ){
      int err = noErr;
      //
      // write an 8 bit tiff with 256 colour colour table, if outTIFFCTab == NULL then do Grayscale
      //
      zFile fileAccess = NULL;  int     imageSize= 0;

#ifdef TIFF_BIGENDIAN_OS
      const int tiffPreHeader0= 0x4d4d002a; // basic bigendian TIFF magic number, chose one!
#endif

#ifdef TIFF_LITENDIAN_OS
      const int tiffPreHeader0= 0x002a4949; // basic littleendian TIFF magic number
#endif

      //
      // fixed header order and offsets.  Preheader, resolution, ctab, tags then data
      //
      const int tiffPreHeader1      = 0x00000618; // offset to tags
      const int tiffXResHeader0     = 0x00000048; // 72 pix/inch default, numerator
      const int tiffXResHeader1     = 0x00000001; // denominator => 72.0 = 72/1
      const int tiffImageDataOffset = 0x000006ae; // specific to this header
      const short nTPS              = 0x000C;     // 12 tags in IFD
      const int ifdTermintator      = 0x00000000; // all TIFF blocks end in 4 bytes of 0

      TagStructure tiffTag;

      fileAccess = meas_open( aFile, "wb");
      if (fileAccess) {

            imageSize = (nxOut*nyOut);

            meas_write( fileAccess, (void*)&tiffPreHeader0, 4UL );
            meas_write( fileAccess, (void*)&tiffPreHeader1, 4UL );

            meas_write( fileAccess, (void*)&tiffXResHeader0, 4UL ); // X-resolution Rational long1/long2
            meas_write( fileAccess, (void*)&tiffXResHeader1, 4UL );
            meas_write( fileAccess, (void*)&tiffXResHeader0, 4UL ); // Y-resolution Rational long1/long2
            meas_write( fileAccess, (void*)&tiffXResHeader1, 4UL );

            meas_write( fileAccess, outTIFFCTab, sizeof(TIFFCTab) );

            meas_write( fileAccess, (void*)&nTPS, 2 );

            tiffTag.tCode = 0x0100; // width code
            tiffTag.dataType = 4; // long
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = nxOut;

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


            tiffTag.tCode = 0x0101; // height code
            tiffTag.dataType = 4; // long
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = nyOut;

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x0102; // bits per pixel code
            tiffTag.dataType = 3;
            tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
            tiffTag.pDataField = 0x00080000; // 8 bits per pixel
#endif
#ifdef TIFF_LITENDIAN_OS
            tiffTag.pDataField = 0x00000008; // 8 bits per pixel
#endif

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x0103; // compression code
            tiffTag.dataType = 3; // short
            tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
            tiffTag.pDataField = 0x00010000; //none compression
#endif
#ifdef TIFF_LITENDIAN_OS
            tiffTag.pDataField = 0x00000001; //none compression
#endif

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x0106; // Perception Intent code
            tiffTag.dataType = 3; // short
            tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
            tiffTag.pDataField = 0x00030000; // 1 => 0 = black GS,  3=> colour TABLE
#endif
#ifdef TIFF_LITENDIAN_OS
            tiffTag.pDataField = 0x00000003; // 1 => 0 = black GS,  3=> colour TABLE
#endif

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x0115; // samples per pix
            tiffTag.dataType = 3; // short
            tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
            tiffTag.pDataField = 0x00010000; // 1 channel
#endif
#ifdef TIFF_LITENDIAN_OS
            tiffTag.pDataField = 0x00000001; // 1 channel
#endif

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


            tiffTag.tCode = 0x0116; // Rows Per Strip (all in one block!)
            tiffTag.dataType = 4; // long
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = nyOut; // 1 channel

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


            tiffTag.tCode = 0x0117; // StripByteCounts
            tiffTag.dataType = 4; // long
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = imageSize; // 1 channel

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


            tiffTag.tCode = 0x0111; // strip offset from start of whole file
            tiffTag.dataType = 4; // long
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = tiffImageDataOffset;

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x011A; // X-resolution rational offset back to preheader
            tiffTag.dataType = 5;
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = 0x00000008;

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x011B; // Y-resolution rational offset back to preheader
            tiffTag.dataType = 5;
            tiffTag.nDataValues = 1;
            tiffTag.pDataField = 0x00000010;

            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            tiffTag.tCode = 0x0140; // colour map/table
            tiffTag.dataType = 3;   // SHORT
            tiffTag.nDataValues = 3*256;
            tiffTag.pDataField = 0x00000018; // location of table n bytes from SOF
            meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

            meas_write( fileAccess, (void*)&ifdTermintator, 4UL );

            // at last- write the data bytes out....

            err      =meas_write( fileAccess, img, imageSize );

            meas_close(fileAccess);

      } else { err = fileErr; }

      return (err);

}


int Write_tif8RGB     (char * aFile,  unsigned char * imgR, unsigned char * imgG, unsigned char * imgB, int nxOut, int nyOut ){
      int err = noErr;
      //
      // write an 8 bit/channel RGB tiff  grayscale
      //
      zFile fileAccess = NULL;  int     imageSize= 0;
      unsigned char  *imgRGB = NULL;
      int    i, j, jOffset;


#ifdef TIFF_BIGENDIAN_OS
      const int tiffPreHeader0= 0x4d4d002a; // basic bigendian TIFF magic number, chose one!
#endif

#ifdef TIFF_LITENDIAN_OS
      const int tiffPreHeader0= 0x002a4949; // basic littleendian TIFF magic number
#endif

      //
      // fixed header order and offsets.  Preheader, resolution, ctab, tags then data
      //
      const int tiffPreHeader1      = 0x0000001E; // offset to tags
      const int tiffXResHeader0     = 0x00000048; // 72 pix/inch default, numerator
      const int tiffXResHeader1     = 0x00000001; // denominator => 72.0 = 72/1
      const int tiffImageDataOffset = 0x000000a8; // specific to this header
      const short tiffChannelBits   = 0x0008;     // for 8,8,8 array, bits per rgb
      const short nTPS              = 0x000B;     // 11 tags in IFD
      const int ifdTermintator      = 0x00000000; // all TIFF blocks end in 4 bytes of 0

      TagStructure tiffTag;


      // allocate rgb chunky pixel array an combine rgb planes
      imageSize = 3L*nxOut*nyOut;
      imgRGB = (unsigned char *) malloc(imageSize);

      if (imgRGB != NULL) {

            for (j = 0; j<nyOut; j++){
                  jOffset = nxOut*j;
                  for (i = 0; i<nxOut; i++){

                        imgRGB[3*(i + jOffset)  ] = imgR[i + jOffset];
                        imgRGB[3*(i + jOffset)+1] = imgG[i + jOffset];
                        imgRGB[3*(i + jOffset)+2] = imgB[i + jOffset];

                  }//i
            }//j


            fileAccess = meas_open( aFile, "wb");
            if (fileAccess) {

                  meas_write( fileAccess, (void*)&tiffPreHeader0, 4UL );
                  meas_write( fileAccess, (void*)&tiffPreHeader1, 4UL );

                  meas_write( fileAccess, (void*)&tiffXResHeader0, 4UL ); // X-resolution Rational long1/long2
                  meas_write( fileAccess, (void*)&tiffXResHeader1, 4UL );

                  meas_write( fileAccess, (void*)&tiffXResHeader0, 4UL ); // Y-resolution Rational long1/long2
                  meas_write( fileAccess, (void*)&tiffXResHeader1, 4UL );

                  meas_write( fileAccess, (void*)&tiffChannelBits, 2 ); // 8 bits per red
                  meas_write( fileAccess, (void*)&tiffChannelBits, 2 ); // 8 bits per green
                  meas_write( fileAccess, (void*)&tiffChannelBits, 2 ); // 8 bits per blue

                  meas_write( fileAccess, (void*)&nTPS, 2 );

                  tiffTag.tCode = 0x0100; // width code
                  tiffTag.dataType = 4; // long
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = nxOut;

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


                  tiffTag.tCode = 0x0101; // height code
                  tiffTag.dataType = 4; // long
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = nyOut;

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  tiffTag.tCode = 0x0102; // bits per pixel code
                  tiffTag.dataType = 3;
                  tiffTag.nDataValues = 3;
                  tiffTag.pDataField = 0x00000018; //offset to bbp table

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  tiffTag.tCode = 0x0103; // compression code
                  tiffTag.dataType = 3; // short
                  tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
                  tiffTag.pDataField = 0x00010000; // none compression
#endif
#ifdef TIFF_LITENDIAN_OS
                  tiffTag.pDataField = 0x00000001; // none compression
#endif

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  tiffTag.tCode = 0x0106; // Perception Intent code
                  tiffTag.dataType = 3; // short
                  tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
                  tiffTag.pDataField = 0x00020000; // 1 => 0 = black GS, 2=> RGB, 3=> colour TABLE
#endif
#ifdef TIFF_LITENDIAN_OS
                  tiffTag.pDataField = 0x00000002; // 1 => 0 = black GS, 2=> RGB, 3=> colour TABLE
#endif

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  tiffTag.tCode = 0x0115; // samples per pix
                  tiffTag.dataType = 3; // short
                  tiffTag.nDataValues = 1;
#ifdef TIFF_BIGENDIAN_OS
                  tiffTag.pDataField = 0x00030000; // 3 channels
#endif
#ifdef TIFF_LITENDIAN_OS
                  tiffTag.pDataField = 0x00000003; // 3 channels
#endif

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


                  tiffTag.tCode = 0x0116; // Rows Per Strip (all in one block! nya nya! 8k sheesh!)
                  tiffTag.dataType = 4; // long
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = nyOut; // 1 channel

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


                  tiffTag.tCode = 0x0117; // StripByteCounts
                  tiffTag.dataType = 4; // long
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = imageSize; // 1 channel

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );


                  tiffTag.tCode = 0x0111; // strip offset from start of whole file
                  tiffTag.dataType = 4; // long
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = tiffImageDataOffset;

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  tiffTag.tCode = 0x011A; // X-resolution rational offset back to preheader
                  tiffTag.dataType = 5;
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = 0x00000008;

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  tiffTag.tCode = 0x011B; // Y-resolution rational offset back to preheader
                  tiffTag.dataType = 5;
                  tiffTag.nDataValues = 1;
                  tiffTag.pDataField = 0x00000010;

                  meas_write( fileAccess, (void*)&tiffTag, sizeof(TagStructure) );

                  meas_write( fileAccess, (void*)&ifdTermintator, 4UL );

                  // at last- write the data bytes out....

                  err      =meas_write( fileAccess, imgRGB, imageSize );

                  meas_close(fileAccess);

            } else { err = fileErr;}

            free(imgRGB);

      } // imgRGB allocated OK
      else { err = memErr;}

      return (err);

}

//----------------------------------------------------------------------------------------------------------------
//
// PNG File IO Routines
//
//----------------------------------------------------------------------------------------------------------------
//
// Only write operations are used.
//
//========================================

#define ZLS_PNGINTERLACE  (PNG_INTERLACE_NONE)
#define ZLS_PNGCOMPRESS   (PNG_COMPRESSION_TYPE_BASE)
#define ZLS_PNGFILTER     (PNG_FILTER_TYPE_BASE)
#define ZLS_PNGTRANS      (PNG_TRANSFORM_IDENTITY)

// here all PNGs,have 8 bits per channel

//================================================
// 8 bit single channel with colour lookup table
//================================================

int Write_png8     (char * aFile, unsigned char * img, TIFFCTab * outTIFFCTab, int nxOut, int nyOut ){

      int err = noErr;

      FILE * fileAccess = NULL;
      png_structp png_ptr;
      png_infop info_ptr;
      png_colorp palette;

      fileAccess = fopen( aFile, "wb");
      if (fileAccess) {

            png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

            if (png_ptr == NULL)  {
                  fclose(fileAccess);
                  return (fileErr);
            }

            info_ptr = png_create_info_struct(png_ptr);
            if (info_ptr == NULL) {
                  fclose(fileAccess);
                  png_destroy_write_struct(&png_ptr,  NULL);
                  return (fileErr);
            }

            png_init_io(png_ptr, fileAccess); // associate file with png_

            png_set_IHDR( png_ptr, info_ptr, nxOut, nyOut, 8,
                         PNG_COLOR_TYPE_PALETTE, ZLS_PNGINTERLACE, ZLS_PNGCOMPRESS, ZLS_PNGFILTER );

            palette = ( png_colorp) png_malloc(png_ptr, ( PNG_MAX_PALETTE_LENGTH *sizeof (png_color)) );

            int i;
            for (i = 0; i < PNG_MAX_PALETTE_LENGTH; i++){
                  palette[i].red   = (unsigned char)((outTIFFCTab->red[i])>>8);
                  palette[i].green = (unsigned char)((outTIFFCTab->green[i])>>8);
                  palette[i].blue  = (unsigned char)((outTIFFCTab->blue[i])>>8);
            }//i

            png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);

            png_write_info(png_ptr, info_ptr);

            png_uint_32 row_stride = nxOut; /* per row in image_buffer */
            png_uint_32 k;
            png_bytep row_pointers[nyOut];

            for (k = 0; k < nyOut; k++)
                  row_pointers[k] = img + k*row_stride;

            png_write_image(png_ptr, row_pointers);

            png_write_end(png_ptr, info_ptr);

            png_destroy_write_struct(&png_ptr, &info_ptr);

            fclose(fileAccess);

      } else { err = fileErr;}

      return (err);

}

//================================================
// 8 bit three channel RGB files
//================================================

int Write_png8RGB     (char * aFile, unsigned char * imgR, unsigned char * imgG, unsigned char * imgB, int nxOut, int nyOut ){
      
      int err = noErr;
      
      unsigned char  *imgRGB = NULL;
      
      FILE * fileAccess = NULL;
      png_structp png_ptr;
      png_infop info_ptr;
      
      
      // allocate rgb chunky pixel array and combine rgb planes
      
      imgRGB = (unsigned char *) malloc(3L*nxOut*nyOut);
      
      if (imgRGB != NULL) {
            
            int i, j, jOffset;
            
            for (j = 0; j<nyOut; j++){
                  jOffset = nxOut*j;
                  for (i = 0; i<nxOut; i++){
                        
                        imgRGB[3*(i + jOffset)  ] = imgR[i + jOffset];
                        imgRGB[3*(i + jOffset)+1] = imgG[i + jOffset];
                        imgRGB[3*(i + jOffset)+2] = imgB[i + jOffset];
                        
                  }//i
            }//j
            
            
            fileAccess =  fopen( aFile, "wb");
            
            if (fileAccess) {
                  
                  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
                  
                  if (png_ptr == NULL)   {
                        free(imgRGB);
                        fclose(fileAccess);
                        return (fileErr);
                  }
                  
                  info_ptr = png_create_info_struct(png_ptr);
                  if (info_ptr == NULL)  {
                        free(imgRGB);
                        fclose(fileAccess);
                        png_destroy_write_struct(&png_ptr,  NULL);
                        return (fileErr);
                  }
                  
                  png_init_io(png_ptr, fileAccess); // associate file with png_
                  
                  png_set_IHDR( png_ptr, info_ptr, nxOut, nyOut, 8, 
                               PNG_COLOR_TYPE_RGB, ZLS_PNGINTERLACE, ZLS_PNGCOMPRESS, ZLS_PNGFILTER );
                  
                  png_write_info(png_ptr, info_ptr);
                  
                  png_uint_32 row_stride = nxOut * 3; /* per row in image_buffer */
                  png_uint_32 k;
                  png_bytep row_pointers[nyOut];
                  
                  for (k = 0; k < nyOut; k++)
                        row_pointers[k] = imgRGB + k*row_stride;
                  
                  png_write_image(png_ptr, row_pointers);
                  
                  png_write_end(png_ptr, info_ptr);
                  
                  png_destroy_write_struct(&png_ptr, &info_ptr);
                  
                  fclose(fileAccess);
                  
            } else { err = fileErr;}
            
            free(imgRGB);
            
      } // imgRGB allocated OK
      else { err = memErr;}
      
      return (err);
      
}



